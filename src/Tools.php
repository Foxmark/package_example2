<?php

namespace Foxmark;


class Tools {
    protected $prop;

    public function __construct($prop = false)
    {
        if($prop) {
            $this->prop = $prop;
        }
    }

    public function run()
    {
        echo 'Calling: ' . __METHOD__ . PHP_EOL;
    }

    public function getProp()
    {
        return $this->prop;
    }
}