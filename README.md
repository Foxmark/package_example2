# Foxmark Tools v1.1.5

### Description
This is an example of unofficial composer package setup based on cvs tag functionality
where every tag represents new software version like: v1.0.0 or v.1.1.0

_To read more about tags click:_ [here](https://www.atlassian.com/git/tutorials/inspecting-a-repository/git-tag)

### Installation
From root of your project run:

`composer config repositories.foxmark-example2 vcs git@bitbucket.org:Foxmark/package_example2.git`

and 

```composer require foxmark/package-example-1:v1.0.0```

or

```composer require foxmark/package-example-1:v1.1.0```

_Note: See how after switching to 1.1.0 version you will be able to use public getProp method
and version 1.1.5 will only add this README file._

You can also create `composer.json` file in root of your project and paste:
```
{
     "repositories": {
         "foxmark-example1": {
             "type": "vcs",
             "url": "git@bitbucket.org:Foxmark/package_example2.git"
         }
     },
     "require": {
         "foxmark/package-example-1": "1.0.0"
     }
 }
```
save file and run: `composer install`

Your application code could look like this:

```
<?php

require_once 'vendor/autoload.php';

use Foxmark\Tools as Tools;

$obj = new Tools('Hello World!');
if(method_exists($obj,'getProp')) {
    var_dump($obj->getProp());
} else {
   echo 'To use getProp method switch to BETA branch (dev-beta) in your composer.json file.' . PHP_EOL;
}

```